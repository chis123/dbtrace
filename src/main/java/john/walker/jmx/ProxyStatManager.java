package john.walker.jmx;

/**
 * @author Johnnie Walker
 *
 */
public class ProxyStatManager implements ProxyStatManagerMBean {

	private final static ProxyStatManager      instance       = new ProxyStatManager();

	public static ProxyStatManager getInstance() {
		return instance;
	}

	private boolean printLog = true;

	@Override
	public void close() {
		this.setPrintLog(false);
	}

	@Override
	public void open() {
		this.setPrintLog(true);
	}

	public boolean isPrintLog() {
		return printLog;
	}

	public void setPrintLog(boolean printLog) {
		this.printLog = printLog;
	}

}
